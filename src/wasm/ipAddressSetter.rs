use crate::wasm::validators::validate_network_address;

pub fn set_interface_ip_address(interface_name: &str, interface_ip_adress: &[u8; 4], interface_mask: &[u8; 4], enable: &bool) -> String {
	if validate_network_address(*interface_ip_adress, *interface_mask) == true{
		return String::from("E: Interface has set valid network address. Change it to something else");
	}
	let mut interface_config: String = format!("interface {}\n", interface_name);
	interface_config.push_str(&*format!("ip address {}.{}.{}.{} {}.{}.{}.{}\n{}shutdown\nexit\n",
	                                    interface_ip_adress[0], interface_ip_adress[1], interface_ip_adress[2], interface_ip_adress[3],
	                                    interface_mask[0], interface_mask[1], interface_mask[2], interface_mask[3],
	                                    if *enable==false { "" } else { "no " }
	));

	interface_config
}