use crate::wasm::validators::validate_network_address;

pub fn generate_static_route(network_addr: &[u8; 4], network_mask: &[u8; 4], next_hop: &[u8; 4]) -> String {
	if !validate_network_address(*network_addr, *network_mask) {
		return format!("E: Address of network {}.{}.{}.{} with mask {}.{}.{}.{} isn't valid!",
		               network_addr[0], network_addr[1], network_addr[2], network_addr[3],
		               network_mask[0], network_mask[1], network_mask[2], network_mask[3]
		);
	}
	format!("ip route {}.{}.{}.{} {}.{}.{}.{} {}.{}.{}.{}\n",
	               network_addr[0], network_addr[1], network_addr[2], network_addr[3],
	               network_mask[0], network_mask[1], network_mask[2], network_mask[3],
	               next_hop[0], next_hop[1], next_hop[2], next_hop[3]
	)
}
