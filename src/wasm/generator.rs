use crate::wasm::routing::generate_static_route;
use crate::wasm::ciscoBase::create_basic_conf;
use crate::wasm::ipAddressSetter::set_interface_ip_address;

pub struct CiscoConf {
	conf: String,
}

impl CiscoConf {
	pub fn new() -> CiscoConf{
		CiscoConf{
			conf: String::from("")
		}
	}

	pub fn get_conf(&mut self) -> &str {
		self.conf.as_str()
	}

	pub fn add_base(&mut self, hostname: &str, password: &str, mask: &bool){
		self.conf.push_str(&*create_basic_conf(hostname, password, mask));
	}

	pub fn add_static_route(&mut self, network_addr: &[u8; 4], network_mask: &[u8; 4], next_hop: &[u8; 4]) {
		self.conf.push_str(&*generate_static_route(network_addr, network_mask, next_hop));
	}

	pub fn add_physical_interface(&mut self, interface_name: &str, interface_address: &[u8;4], interface_mask: &[u8;4], enable: &bool){
		self.conf.push_str(&*set_interface_ip_address(interface_name, interface_address, interface_mask, enable))
	}
}