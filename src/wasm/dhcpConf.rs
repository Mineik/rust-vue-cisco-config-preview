pub fn with_dhcp(pool_name: &str,
                 router_gateway: &[u8; 4],
                 network_pool_adress: &[u8; 4],
                 network_pool_mask: &[u8; 4],
                 use_dns: bool,
                 dns_ip_adress: &[u8; 4]) -> String {
	let mut dhcp_config_lines: String = format!("dhcp pool {}\n", pool_name);
	dhcp_config_lines.push_str(&*format!("network {}.{}.{}.{} {}.{}.{}.{}\n",
	                                     network_pool_adress[0], network_pool_adress[1], network_pool_adress[2], network_pool_adress[3],
	                                     network_pool_mask[0], network_pool_mask[1], network_pool_mask[2], network_pool_mask[3]
	));
	dhcp_config_lines.push_str(&*format!("default-router {}.{}.{}.{}\n", router_gateway[0], router_gateway[1], router_gateway[2], router_gateway[3]));
	if use_dns {
		dhcp_config_lines.push_str(&*format!("dns-server {}.{}.{}.{}\n", dns_ip_adress[0], dns_ip_adress[1], dns_ip_adress[2], dns_ip_adress[3]));
	}

	dhcp_config_lines.push_str(&*format!("exit\n"));

	dhcp_config_lines
}
