use std::net::Ipv4Addr;

pub fn validate_network_address(network_address: [u8;4], network_mask: [u8;4]) -> bool{
	let net_addr: u32 = Ipv4Addr::from(network_address).into();
	let net_mask: u32 = Ipv4Addr::from(network_mask).into();

	net_addr & net_mask == net_addr
}