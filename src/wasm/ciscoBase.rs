pub fn create_basic_conf(hostname: &str, password: &str, is_secret: &bool) -> String {
	let mut str: String = format!("enable\nconfigure terminal\nhostname {}\n", hostname);
	if *is_secret {
		str.push_str(&*format!("enable secret {}\n", password));
	} else {
		str.push_str(&*format!("enable password {}\n", password));
	}

	str
}

pub fn generate_motd_banner(banner: &str) ->String{
	format!("banner motd !\n{}\n!\n", banner)
}

pub fn enable_router_telnet(telnet_password: &str) -> String {
	let mut str: String = "line vty 0 15\n".to_owned();
	str.push_str(&*format!("transport telnet"));
	str.push_str(&*format!("password {}\n", telnet_password));

	str
}

