mod wasm;
mod utils;

use wasm_bindgen::prelude::*;
use web_sys::console;
use crate::wasm::*;
use crate::wasm::generator::CiscoConf;


#[wasm_bindgen(start)]
pub fn js_test() {
	utils::set_panic_hook();
	console::log_1(&JsValue::from_str("Loaded! Running self test to to determine if everything works correctly..."));
	let test_ip_address = [192, 168, 5, 64];
	let test_subnet_mask = [255, 255, 255, 192];
	let result = validators::validate_network_address(test_ip_address, test_subnet_mask);
	if !result {
		console::error_1(&JsValue::from_str(&*format!("Self test failed! network {}.{}.{}.{} with subnet mask {}.{}.{}.{} was deemed invalid",
		                                              test_ip_address[0], test_ip_address[1], test_ip_address[2], test_ip_address[3],
		                                              test_subnet_mask[0], test_subnet_mask[1], test_subnet_mask[2], test_subnet_mask[3]
		)))
	} else {
		console::log_1(&JsValue::from_str("Self test passed!"))
	}
	console::log_1(&JsValue::from_str("Done!"))
}

#[wasm_bindgen(js_name = genCisco)]
pub fn gen_cisco(hostname: &str, password: &str, is_secret: bool) {
	console::log_1(&JsValue::from("Generating cisco config..."));

	console::log_1(&JsValue::from("Done!"))
}

#[wasm_bindgen(js_name = previewIpRoute)]
pub fn preview_ip_route(network_ip_adress_a: u8,
                        network_ip_address_b: u8,
                        network_ip_address_c: u8,
                        network_ip_address_d: u8,
                        network_mask_a: u8,
                        network_mask_b: u8,
                        network_mask_c: u8,
                        network_mask_d: u8,
                        next_hop_a: u8,
                        next_hop_b: u8,
                        next_hop_c: u8,
                        next_hop_d: u8)
                        -> Result<JsValue, JsValue> {
	let route_box = routing::generate_static_route(&[network_ip_adress_a, network_ip_address_b, network_ip_address_c, network_ip_address_d],
	                                               &[network_mask_a, network_mask_b, network_mask_c, network_mask_d],
	                                               &[next_hop_a, next_hop_b, next_hop_c, next_hop_d]);
	Ok(JsValue::from(String::from(route_box).as_str()))
}

#[wasm_bindgen(js_name = previewInterfaceConf)]
pub fn preview_interface_conf(interface_name: &str,
                              ip_a: u8, ip_b: u8, ip_c: u8, ip_d: u8,
                              mask_a: u8, mask_b: u8, mask_c: u8, mask_d: u8,
                              enable: bool) -> Result<JsValue, JsValue> {
	let prev = ipAddressSetter::set_interface_ip_address(interface_name, &[ip_a, ip_b, ip_c, ip_d], &[mask_a, mask_b, mask_c, mask_d], &enable);
	Ok(JsValue::from(&prev))
}


#[wasm_bindgen]
pub struct Gen {
	generator: CiscoConf,
}

#[wasm_bindgen]
impl Gen {
	pub fn new() -> Gen {
		Gen {
			generator: CiscoConf::new()
		}
	}

	#[wasm_bindgen(js_name = getConfig)]
	pub fn get_config(&mut self) -> Result<String, JsValue> {
		Ok(String::from(self.generator.get_conf()))
	}

	#[wasm_bindgen(js_name = setBase)]
	pub fn set_base(&mut self, hostname: &str, password: &str, mask: bool) {
		self.generator.add_base(hostname, password, &mask);
	}

	#[wasm_bindgen(js_name = newStaticRoute)]
	pub fn new_static_route(&mut self,
	                        network_address_a: u8,
	                        network_address_b: u8,
	                        network_address_c: u8,
	                        network_address_d: u8,
	                        network_mask_a: u8,
	                        network_mask_b: u8,
	                        network_mask_c: u8,
	                        network_mask_d: u8,
	                        next_hop_a: u8,
	                        next_hop_b: u8,
	                        next_hop_c: u8,
	                        next_hop_d: u8,
	) {
		self.generator.add_static_route(
			&[network_address_a, network_address_b, network_address_c, network_address_d],
			&[network_mask_a, network_mask_b, network_mask_c, network_mask_d],
			&[next_hop_a, next_hop_b, next_hop_c, next_hop_d],
		);
	}

	#[wasm_bindgen(js_name = newPhysicalInterface)]
	pub fn new_physical_interface(&mut self,
	                              interface_name: &str,
	                              network_address_a: u8,
	                              network_address_b: u8,
	                              network_address_c: u8,
	                              network_address_d: u8,
	                              network_mask_a: u8,
	                              network_mask_b: u8,
	                              network_mask_c: u8,
	                              network_mask_d: u8,
	                              enable: bool,
	) {
		self.generator.add_physical_interface(
			interface_name,
			&[network_address_a, network_address_b, network_address_c, network_address_d],
			&[network_mask_a, network_mask_b, network_mask_c, network_mask_d],
			&enable,
		);
	}
}