import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {load as loadWasmPlugin} from  "./plugins/WASM"

Vue.config.productionTip = false

loadWasmPlugin().then(()=>{
new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
})

