import {VueConstructor} from "vue-class-component/dist/vue-class-component";
import Vue from "vue"
import type WASMTypes from "@/assets/pkg/index"

declare module 'vue/types/vue' {
	interface Vue {
		$rustWasm: typeof WASMTypes
	}
}

export interface RustWasmReaderOpts {
	importedModule: any
}

export const RustWasmReaderPlugin = {
	install: (Vue: VueConstructor, options: RustWasmReaderOpts) => {
		Vue.prototype.$rustWasm = options.importedModule
	}
}

export async function load() {
	const module = await import("@/assets/pkg")
	// @ts-ignore
	Vue.use(RustWasmReaderPlugin, {
		importedModule: module
	})
}