/*  eslint-disable */

const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin")
const  path = require("path")

module.exports = {
	pluginOptions: {
		autoRouting: {
			chunkNamePrefix: 'page-'
		}
	},

	configureWebpack : {
		plugins :[
			new WasmPackPlugin({
				crateDirectory: path.resolve(__dirname, "src"),
				args: "--log-level warn",
				outDir: "./src/assets/pkg",
				forceWatch: process.env.NODE_ENV==="development"
			})
		]
	}
}
