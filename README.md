# Cisco IOS router config generator

Uses rust and WASM power to generate cisco ios router configuration

It definitely isn't perfect and capable of doing 100% of stuff, but it is great for basic stuff.

## Prerequisites
* cargo
* rust
* wasm-pack

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```